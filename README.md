## What are these smart contracts for?
Vesting smart contracts are used for LP tokens staking and Yel tokens rewarding.
Additional contracts just used to be deployed Chromosome_BOO_FTM, GnG, YELToken

## Install requirements
1. `npm install`
2. `npm install -g npx`

## Configuration
1. Create `./secrets.json` file in the root directory
2. Then you should put there keys information (these keys needs for deployment)
```
{
  "privateKey": "0xaf2f456a4d...",
  "etherscanApiKey": "I9YNV9YWM84IY...",
  "infuraKey": "f52..."
}
```

## Deployment
The `scripts/deploy_*.js` files use for contracts deployment.

**Using ftm**
`npx hardhat run scripts/deploy_*.js --network ftm`

**Using bsc**
`npx hardhat run scripts/deploy_*.js --network bsc`

**Using hardhat**
`npx hardhat run scripts/deploy_*.js --network hardhat`

**Using rinkeby**
`npx hardhat run scripts/deploy_*.js --network rinkeby`

**Using mainnet**
`npx hardhat run scripts/deploy_*.js --network mainnet`

**Examples with results**:

- Deployment:

```
CH BOO FTM with 1 percent deployed to: 0xBB6828C8228E5C641Eb6d89Ca22e09E6311CA398
```

## Testing
To run tests for contracts you should run a hardhat node `npx hardhat node` in the console using the first tab and using another one you should execute `npx hardhat test`


## Verfification
To make quick verification use `npx hardhat verify` command with parameters `--network` and address of the contract.
Like: `npx hardhat verify --network <NAME_OF_NETWORK> <ADDRESS_OF_CONTRACT> <CONSTRUCTOR_PARAMETER1> <CONSTRUCTOR_PARAMETER1> ...`

Example:
```
npx hardhat verify --network ftm 0x43168ef897D943230bDF62bF6231C084f2F18F1d "0xd3b71117e6c1558c1553305b44988cd944e97300" "0x841fad6eae12c286d1fd18d1d525dffa75c7effe" "0xec7178f4c41f346b2721907f5cf7628e388a7a58" "0x2b2929e785374c651a81a63878ab22742656dcdd" 0
```
