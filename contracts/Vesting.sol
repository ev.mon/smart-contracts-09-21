// SPDX-License-Identifier: MIT

/*
 *       $$$$$$_$$__$$__$$$$__$$$$$$
 *       ____$$_$$__$$_$$_______$$
 *       ____$$_$$__$$__$$$$____$$
 *       $$__$$_$$__$$_____$$___$$
 *       _$$$$___$$$$___$$$$____$$
 *
 *       $$__$$_$$$$$$_$$$$$__$$_____$$$$$
 *       _$$$$____$$___$$_____$$_____$$__$$
 *       __$$_____$$___$$$$___$$_____$$__$$
 *       __$$_____$$___$$_____$$_____$$__$$
 *       __$$___$$$$$$_$$$$$__$$$$$$_$$$$$
 *
 *       $$___$_$$$$$$_$$$$$$_$$__$$
 *       $$___$___$$_____$$___$$__$$
 *       $$_$_$___$$_____$$___$$$$$$
 *       $$$$$$___$$_____$$___$$__$$
 *       _$$_$$_$$$$$$___$$___$$__$$
 *
 *       $$__$$_$$$$$__$$
 *       _$$$$__$$_____$$
 *       __$$___$$$$___$$
 *       __$$___$$_____$$
 *       __$$___$$$$$__$$$$$$
 */

pragma solidity ^0.8.4;

import "./SafeERC20.sol";
import "./IERC20.sol";
import "./Ownable.sol";


contract Vesting is Ownable {
    using SafeERC20 for IERC20;

    IERC20 immutable public usedToken;

    uint256 immutable public creationTime;
    uint256 immutable public endTime;
    uint256 public totalTokensReleased = 0; // amount of tokens that released from this contract
    uint256 SCALING_FACTOR = 10 ** 18; // decimals
    
    uint256 public vestingPeriod = 26;
    uint256 SECONDS_IN_WEEK = 604800;
    uint256 public vestingPeriodTime = vestingPeriod * SECONDS_IN_WEEK; // seconds in 26 week
    
    // Vesting information for a certain address
    mapping(address => VestingInfo) public vestingInfo;

    struct VestingInfo {
        uint256 totalAmount; // amount that should be released
        uint256 sent; // how much tokens already sent to the user
        uint256 lastTime; // last time of request
        bool released; // a flag that verifies if funds were already withdrawn
        bool isSet; // a flag verifies that amount already set for current Vesting
    }

    event TokenVestingCreated(address indexed receiver, uint256 amount);
    event TokenVestingReleased(address indexed receiver, uint256 amount);
    event TokenVestingRemoved(address indexed receiver);

    constructor (IERC20 _token) {
        require(address(_token) != address(0x0), "Vesting: token address is not valid");
        usedToken = _token;
        creationTime = block.timestamp;
        endTime = block.timestamp + vestingPeriodTime;
    }

    // In a case when there are some YEL tokens left or they are don't need anymore
    // on a contract this function allows the owner to retrieve excess tokens
    function retrieveAccessTokens(uint256 _amount) external onlyOwner {
        require(
            _amount <= usedToken.balanceOf(address(this)),
            "Vesting: balance of the contract is less then provided amount"
        );
        usedToken.safeTransfer(owner(), _amount);
    }

    function addVestingInfoBulk(address[] memory _addresses, uint256[] memory _amounts) public onlyOwner {
        _checkParameters(_addresses, _amounts);

        for(uint256 i; i < _addresses.length; i++) {
            _addVestingInfo(_addresses[i], _amounts[i]);
        }
    }

    // Adds vesting information to vesting array. Can be called only by contract owner
    function removeVesting(address _address) external onlyOwner {
        VestingInfo storage vesting = vestingInfo[_address];
        require(vesting.isSet, "Vesting: amount of tokens was not set yet for this address");
        require(!vesting.released, "Vesting: amount of tokens already released");
        vesting.totalAmount = 0;
        vesting.released = true;
        emit TokenVestingRemoved(_address);
    }

    // Function is responsible for releasing the funds at specific point of time
    function release() public {
        VestingInfo storage vesting = vestingInfo[msg.sender];
        require(!vesting.released, "Vesting: amount of tokens already released");
        require(vesting.isSet, "Vesting: amount of tokens was not set for this address");

        // how many tokens for receiver per one week
        uint256 amountPerWeek = vesting.totalAmount / vestingPeriod;
        // how many days gone from the creationTime
        uint256 deltaTime = block.timestamp - creationTime;
        uint256 vestingAmount = 0;
        
        if (block.timestamp <= endTime) {
            require(
                vesting.sent < (deltaTime / SECONDS_IN_WEEK + 1) * amountPerWeek,
                "Vesting: Tokens were already sent this week"
            );
            vestingAmount = (deltaTime / SECONDS_IN_WEEK + 1) * amountPerWeek - vesting.sent;

            if(vesting.totalAmount == vesting.sent + vestingAmount) {
                vesting.released = true;
            }
        } else {
            vestingAmount = vesting.totalAmount - vesting.sent;
            vesting.released = true;
        }

        vesting.sent += vestingAmount;
        require(
            usedToken.balanceOf(address(this)) >= vestingAmount,
            "Vesting: balance of the contract is less then expected amount"
        );
        usedToken.safeTransfer(msg.sender, vestingAmount);
        totalTokensReleased += vestingAmount;
        vesting.lastTime = block.timestamp;
        emit TokenVestingReleased(msg.sender, vestingAmount);
    }

    function _checkParameters(address[] memory _addresses, uint256[] memory _amounts) internal pure {
        require(
            _amounts.length <= 100, 
            "Vesting: length of provided addresses and amounts should be no more then 100"
        );
        require(
            _amounts.length == _addresses.length,
            "Vesting: length of provided addresses and amounts should be the same"
        );

        for(uint256 i; i < _addresses.length; i++) {
            require(
               _addresses[i] != address(0x0),
                "Vesting: a receiver can not be a zero address"
            );
            require(_amounts[i] != 0, "Vesting: amount can not be 0");
        }
    }

    function _addVestingInfo(address _address, uint256 _amount) internal {
        require(!vestingInfo[_address].isSet, "Vesting: amount of tokens has already set for the user");
        vestingInfo[_address] = VestingInfo(_amount, 0, creationTime, false, true);
        emit TokenVestingCreated(_address, _amount);
    }
}
