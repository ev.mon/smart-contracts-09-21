// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "./ERC721.sol";
import "./ERC721URIStorage.sol";
import "./Ownable.sol";
import "./Counters.sol";


contract NFT is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter public tokenIdTracker;

    uint256 public totalCap;
    uint256[] public mintedTokenList;

    address public saleAgent = address(0);

    string public defaultImageInfo = "https://gateway.pinata.cloud/ipfs/QmWBF9Yvdx7DwfjLEMrFZ1cHda1puemwGnx4JBS82rBXPM";
    string public baseURIPrefix;

    constructor(string memory name_, string memory symbol_, uint256 _totalCap) ERC721(name_, symbol_) {
        totalCap = _totalCap;
    }

    /**
     * @dev Throws if called by any account other than the owner or token sale contract.
     */
    modifier onlyOwnerOrSaleAgent() {
        require(
            msg.sender == saleAgent || msg.sender == owner(),
            "Ownable: caller is not the owner or the sale agent"
        );
        _;
    }

    function getIdTracker() public view returns(uint256) {
        return tokenIdTracker.current();
    }

    function getMintedTokens() public view returns (uint256[] memory) {
        return mintedTokenList;
    }

    function isMinted(uint256 tokenId) public view returns(bool){
        return _exists(tokenId);
    }

    function _baseURI() internal view override returns (string memory) {
        return baseURIPrefix;
    }

    function tokenURI(uint256 tokenId) public view override(ERC721URIStorage) returns (string memory){
        if (bytes(baseURIPrefix).length == 0) {
            return defaultImageInfo;
        } else {
            return super.tokenURI(tokenId);
        }
    }

    function safeMint(address to) public onlyOwnerOrSaleAgent {
        uint256 id = getIdTracker() + 1;
        require(id <= totalCap, "NFT: Can not mint tokens anymore");
        tokenIdTracker.increment();
        _safeMint(to, id);
        mintedTokenList.push(id);
    }

    function setBaseURI(string memory baseURIPrefix_) public onlyOwner {
        baseURIPrefix = baseURIPrefix_;
    }

    function setSaleAgent(address newSaleAgent) external onlyOwnerOrSaleAgent {
        saleAgent = newSaleAgent;
    }
}
