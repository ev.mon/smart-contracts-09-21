// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;


import "./ERC721.sol";
import "./Ownable.sol";
import "./Counters.sol";
import "./NFT.sol";


contract TokenSale is Ownable {
    // Reference to contract tracking NFT ownership
    NFT public nonFungibleContract;
    bool public hasEnded = false;
    uint256 public price = 1 * 1e16; // 0.01 ETH
    uint256 public iterationLimit = 50; // it prevents an error 'out of gas' due to tough transaction
    uint256 public startTime;
    address payable public withdrawAgent;

    constructor(address _nftAddress, uint256 _startTime) {
        require(_nftAddress != address(0x0), "NFT: an NFT address can not be zero address");
        NFT candidateContract = NFT(_nftAddress);
        nonFungibleContract = candidateContract;
        startTime = _startTime;
    }

    modifier saleIsOn {
        require(block.timestamp > startTime,
            "TokenSale: It is not allowed to buy tokens for now"
        );
        require(
            !(nonFungibleContract.getIdTracker() == getTotalSupply()),
            "TokenSale: Sale is over by reaching the total supply"
        );
        _;
    }

    receive() saleIsOn external payable {
       uint256 value = msg.value;

        require(value >= price, "TokenSale: ETH value is not enough for token buying");
        uint256 amount = value / price;

        require(
            amount <= iterationLimit,
            "TokenSale: There is a limit of tokens for each transaction, try to buy less"
        );

        require(
            nonFungibleContract.getIdTracker() + amount <= getTotalSupply(),
            "TokenSale: Tried to buy more then set in total supply."
        );

        for(uint i = 0; i < amount; i++) {
            nonFungibleContract.safeMint(msg.sender);
        }
    }

    function getTotalSupply() public view returns (uint256) {
        return nonFungibleContract.totalCap();
    }

    function withdraw() public onlyOwner {
        require(withdrawAgent != address(0), "TokenSale: Set withdrawAgent address at first");
        withdrawAgent.transfer(address(this).balance);
    }

    function setWithdrawAgent(address payable _newWithdrawAgent) public onlyOwner {
        withdrawAgent = _newWithdrawAgent;
    }
}
