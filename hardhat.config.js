const {
  privateKeyFTM,
  privateKeyRinkeby,
  privateKeyBSC,
  privateKeyMainnet,
  etherscanApiKey,
  alchemyApiKey,
  infuraKey
} = require('./secrets.json');
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-ganache");
require("@nomiclabs/hardhat-web3");
require("solidity-coverage");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.6.6",
      },
      {
        version: "0.6.12",
      },
      {
        version: "0.5.16",
      },
      {
        version: "0.8.9",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          },
          outputSelection: {
            "*": {
                "*": ["storageLayout"],
            },
          }
        }
      }]
  },
  paths: {
    sources: './contracts/vault',
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  defaultNetwork: "hardhat",
  networks: {
  	hardhat: {
      // 'loggingEnabled': true,
      throwOnTransactionFailures: true,
      throwOnCallFailures: true,
      allowUnlimitedContractSize: true,
      // blockGasLimit: 0x1fffffffffffff,
      // accounts: accounts()
    },
    ftm: {
      url: `https://rpc.ftm.tools`,
      chainId: 250,
      gas: 8100000,
      gasPrice: 200000000000,
      // accounts: ["e150698f1623b321be862a883f9c8224aa8a0a323d2edc479b11a0301a6e5bcd"]
      accounts: ["36a6b174519068c9f8c649a108fb8560af8acc99547a41abb6a42aff21a877f6"]
      // accounts: [privateKeyFTM]
    },
    // ten: {
    //   url: `https://rpc.tenderly.co/fork/a706c274-f487-42f6-b526-d43afecf8f05`,
    //   chainId: 250,
    //   gas: 3100000,
    //   gasPrice: 200000000000,
    //   // accounts: ["e150698f1623b321be862a883f9c8224aa8a0a323d2edc479b11a0301a6e5bcd"]
    //   accounts: ["36a6b174519068c9f8c649a108fb8560af8acc99547a41abb6a42aff21a877f6"]
    //   // accounts: [privateKeyFTM]
    // },
    bsc: {
      url: `https://bsc-dataseed.binance.org/`,
      chainId: 56,
      gas: 5543150,
      gasPrice: 5000000000,
      accounts: [privateKeyBSC],

    },
    poly: {
      url: `https://polygon-rpc.com/`,
      chainId: 137,
      gas: 5543150,
      gasPrice: 50000000000,
      accounts: [privateKeyBSC],

    },
    mainnet: {
        url: `https://mainnet.infura.io/v3/${infuraKey}`,
        accounts: [privateKeyMainnet],
        gas: 2000000,
        gasPrice: 80000000000
    },
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${alchemyApiKey}`,
      accounts: [privateKeyRinkeby],
      gas: 8100000
    }
  },
  etherscan: {
    apiKey: etherscanApiKey
  },
  mocha: {
    timeout: 30000
  }
};
