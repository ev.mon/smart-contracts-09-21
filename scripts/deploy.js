const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy.js --network ftm`
async function main() {
    let yel = "0xd3b71117e6c1558c1553305b44988cd944e97300";
    let rewardToken = "0x841fad6eae12c286d1fd18d1d525dffa75c7effe";
    let lpToken = "0xec7178f4c41f346b2721907f5cf7628e388a7a58";
    let farmContract = "0x2b2929e785374c651a81a63878ab22742656dcdd";
    let pid = 0;

    const CH = await hre.ethers.getContractFactory("Chromosome_BOO_FTM");
    const ch = await CH.deploy(yel, rewardToken, lpToken, farmContract, pid);
    await ch.deployed()

    console.log("CH BOO FTM with 1 percent deployed to:", ch.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});