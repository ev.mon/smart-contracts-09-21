const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy_PR_EmiSwap.js --network ftm`
async function main() {
    let esw = "0x5a75a093747b72a0e14056352751edf03518031d";
    let starttime = 1631598756;
    let eswPerSec = "173000000000000000";

    const PR = await hre.ethers.getContractFactory("PR_EmiSwap");
    const pr = await PR.deploy(esw, eswPerSec, starttime);
    await pr.deployed()

    console.log("PR_EmiSwap deployed to:", pr.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});