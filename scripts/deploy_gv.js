const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy.js --network bsc`
async function main() {
    // bsc
    /*
    let yelToken = "0xd3b71117e6c1558c1553305b44988cd944e97300";
    let yelRouter = "0xcf0febd3f17cef5b47b0cd257acf6025c5bff3b7";
    let masterchef = "0x73feaa1ee314f8c655e354234017be2193c9e24e";
    let lpToken = "0x7EFaEf62fDdCCa950418312c6C91Aef321375A00";
    let liqRouter = "0x10ED43C718714eb63d5aA57B78B54704E256024E";
    let pid = 258;
    let nameID = 0;
    let USDT_address = "0x55d398326f99059ff775485246999027b3197955";
    let owner = "0x4e5b3043FEB9f939448e2F791a66C4EA65A315a8";
    let WBNB = "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c";
    */


    // ftm
    
    let yelToken = "0xd3b71117e6c1558c1553305b44988cd944e97300";
    let yelRouter = "0xf491e7b69e4244ad4002bc14e878a34207e38c29";
    let farm = "0x9083ea3756bde6ee6f27a6e996806fbd37f6f093";
    let lpToken = "0xd14Dd3c56D9bc306322d4cEa0E1C49e9dDf045D4";
    let liqRouter = "0x16327e3fbdaca3bcf7e38f5af2599d2ddc33ae52";
    let pid = 17;
    let nameID = 0;
    let USDT_address = "0x049d68029688eAbF473097a2fC38ef61633A3C7A";
    let owner = "0x0C64Ee4d4Ed50293B6709b1fcEa12c0D7CAD88d2";
    let WBNB = "0x21be370d5312f44cb42ce377bc9b8a0cef1a4c83";
    

    // poly
    // let yelToken = "0xd3b71117e6c1558c1553305b44988cd944e97300";
    // let yelRouter = "0xa5e0829caced8ffdd4de3c43696c57f7d7a678ff";
    // let farm = "0x1948abc5400aa1d72223882958da3bec643fb4e5";
    // let lpToken = "0x3324af8417844e70b81555A6D1568d78f4D4Bf1f";
    // let liqRouter = "0x1b02da8cb0d097eb8d57a175b88c7d8b47997506";
    // let pid = 10;
    // let nameID = 1;
    // let USDT_address = "0xc2132d05d31c914a87c6611c10748aeb04b58e8f";
    // // let owner = "0x0C64Ee4d4Ed50293B6709b1fcEa12c0D7CAD88d2";
    // let WBNB = "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270";


    const GrowthVault = await hre.ethers.getContractFactory("GrowthVault");
    const growthVault = await GrowthVault.deploy(
        "Share V", "SV", lpToken, pid, nameID, farm, liqRouter, USDT_address, yelToken, yelRouter);
    await growthVault.deployed();

    console.log("GrowthVault deployed to:", growthVault.address);
    // await growthVault.transferOwnership(owner);

    const signer = await ethers.provider.getSigner("0xA67B07e885D66fDde6A2ABB8c9F2484a340041E5");
    await signer.sendTransaction({
        to: growthVault.address,
        value: ethers.utils.parseEther("1"),
        gasLimit: 3100000
    });
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});
