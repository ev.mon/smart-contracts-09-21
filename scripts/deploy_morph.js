const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy_morph.js --network ftm`
async function main() {
    let morph = "0x0789fF5bA37f72ABC4D561D00648acaDC897b32d";
    let starttime = 1632603078;
    let morphPerSec = "1";

    const MORPH = await hre.ethers.getContractFactory("MORPH");
    const morphContract = await MORPH.deploy(morph, morphPerSec, starttime);
    await morphContract.deployed()

    console.log("MORPH deployed to:", morphContract.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});