const { BigNumber } = require("ethers");
const hre = require("hardhat");



async function main() {
    const totalCap = BigNumber.from(11111);
    const dateStart = 1633936897;

    const NFT = await ethers.getContractFactory("NFT");
    const TokenSale = await hre.ethers.getContractFactory("TokenSale");

    const nft = await NFT.deploy("Test NFT", "NFT", totalCap);
    let nft_contract = await nft.deployed();
    const _nft = await hre.ethers.getContractAt("NFT", nft_contract.address);

    const sale = await TokenSale.deploy(nft.address, dateStart);
    await sale.deployed();
    const _sale = await hre.ethers.getContractAt("TokenSale", sale.address);

    // settings
    await _nft.setSaleAgent(sale.address);
    // await _nft.setBaseURI("https://ipfs.io/ipfs/QmXRh7YQQNNMUbUqQ612BYZEQTh9aFjiDTxgmz6tEBsLAf/");

    const signer = await ethers.provider.getSigner("0x4180B6FD2dc934Cb26AB64c0502A389b4dC142f0")

    await signer.sendTransaction({
        to: sale.address,
        value: ethers.utils.parseEther("0.01"),
        gasLimit: 8100000
    });


    console.log("NFT deployed to:", nft.address);
    console.log("TokenSale deployed to:", sale.address);
    console.log("Settings for contracts were set successfully!");

    
    console.log("Eth sent")


}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
  });
