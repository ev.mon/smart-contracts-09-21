const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy_vault.js --network ftm`
async function main() {
    let lpToken = "0xec7178f4c41f346b2721907f5cf7628e388a7a58";

    const GrowthVault = await hre.ethers.getContractFactory("GrowthVault");
    const growthVault = await GrowthVault.deploy("GV", "GV", lpToken);
    await growthVault.deployed()

    console.log("GrowthVault with 1 percent deployed to:", growthVault.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});