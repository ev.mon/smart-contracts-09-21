const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/deploy.js --network ftm`
async function main() {
    const YELToken = "0xd3b71117e6c1558c1553305b44988cd944e97300";

    const VESTING = await hre.ethers.getContractFactory("Vesting");
    const vest = await VESTING.deploy(YELToken);
    await vest.deployed();

    console.log("Vesting deployed to:", vest.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});