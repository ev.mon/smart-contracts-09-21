const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/ftm/deploy_franc.js --network ftm`
async function main() {
    const GrowthVault = await hre.ethers.getContractFactory("VaultFrankenstein");
    const Strategy = await hre.ethers.getContractFactory("Strategy_MORPH_FTM");
    // const Strategy2 = await hre.ethers.getContractFactory("Strategy_PILLS_MIM");

    // const growthVault = await GrowthVault.deploy();
    // await growthVault.deployed();
    // console.log("VaultFrankenstein:", growthVault.address);

    const strategy = await Strategy.deploy("0xBd52B44c9Dc5Fc5c4F1f49feD5CFCA10fbd052f1");
    await strategy.deployed();

    console.log("Strategy_MORPH_FTM:", strategy.address);
    // const strategy2 = await Strategy2.deploy("0xBd52B44c9Dc5Fc5c4F1f49feD5CFCA10fbd052f1");
    // const strategy2 = await Strategy2.deploy(growthVault.address);
    // await strategy2.deployed();
    
    // console.log("Strategy_PILLS_MIM:", strategy2.address);
    // await growthVault.addStrategy(strategy.address, 0);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});
