const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/ftm/deploy_h2o.js --network ftm`
async function main() {
    const GrowthVault = await hre.ethers.getContractFactory("VaultH2O");
    const Strategy = await hre.ethers.getContractFactory("Strategy_USDC_FTM");
    const Strategy2 = await hre.ethers.getContractFactory("Strategy_DAI_FTM");

    const growthVault = await GrowthVault.deploy();
    await growthVault.deployed();
    console.log("GrowthVault deployed to:", growthVault.address);
    const strategy = await Strategy.deploy(growthVault.address);
    // const strategy = await Strategy.deploy("0xBd52B44c9Dc5Fc5c4F1f49feD5CFCA10fbd052f1");
    await strategy.deployed();
    console.log("Strategy_USDC_FTM:", strategy.address);
    const strategy2 = await Strategy2.deploy(growthVault.address);
    await strategy2.deployed();
    console.log("Strategy_DAI_FTM:", strategy2.address);
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    await growthVault.addStrategy(strategy.address, 0);
    // console.log("--!--")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // console.log("----")
    // await growthVault.addStrategy(strategy2.address, 1);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});
