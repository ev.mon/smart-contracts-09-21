const { BigNumber } = require("ethers");
const hre = require("hardhat");

// `npx hardhat run scripts/ftm/deploy_tesla.js --network ftm`
async function main() {
    const GrowthVault = await hre.ethers.getContractFactory("VaultTesla");
    const Strategy = await hre.ethers.getContractFactory("Strategy_SPELL_fUSDT");
    const Strategy2 = await hre.ethers.getContractFactory("Strategy_SPIRIT_FTM");

    const growthVault = await GrowthVault.deploy();
    await growthVault.deployed();

    const strategy = await Strategy.deploy(growthVault.address);
    await strategy.deployed();
   
    console.log("VaultTesla:", growthVault.address);
    console.log("Strategy_SPELL_fUSDT:", strategy.address);

    const strategy2 = await Strategy2.deploy(growthVault.address);
    await strategy2.deployed();
    console.log("Strategy_SPIRIT_FTM:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    console.log("------------:", strategy2.address);
    await growthVault.addStrategy(strategy.address, 0);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // console.log("------------:", strategy2.address);
    // await growthVault.addStrategy(strategy2.address, 1);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
});
