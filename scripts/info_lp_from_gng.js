/* This script is created for getting all wallets that have positive stakingToken balances
	in the SC(smart contract) from start date of the smart contract (when the SC)
	was created till certain date (T).

	This script save to the file only positive balances during this (T) period of time.

	As the result it stores a .csv file in the results/ directory for each script executing to the SC.
*/

const hre = require("hardhat");
const path = require("path");
const fs = require("fs");
const { resolve } = require("path");
// const csv = require('csv-parser');
const { networks, startblock, endblock }  = require('./config.json');
const https = require('https');
const querystring = require('querystring');
const fetch = require("node-fetch");

const folderCSV = path.join(__dirname, '../results');
const folderLOGS = path.join(__dirname, './logs');
const CURRENT_DATE_TIME = getTimeName();
const ERROR_LOG = `${folderLOGS}/error_${CURRENT_DATE_TIME}.log`;
const MAIN_FILE_NAME = `wallets_info_${CURRENT_DATE_TIME}.csv`;
const network = hre.hardhatArguments.network;

let transactions = [];
let mySet = new Set();

function getTimeName() {
	let date_ob = new Date();

	// current date
	// adjust 0 before single digit date
	let date = ("0" + date_ob.getDate()).slice(-2);

	// current month
	let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

	// current year
	let year = date_ob.getFullYear();

	// current hours
	let hours = date_ob.getHours();

	// current minutes
	let minutes = date_ob.getMinutes();

	// current seconds
	let seconds = date_ob.getSeconds();

	// returns YYYY-MM-DD_HH:MM:SS format
	return (year + "-" + month + "-" + date + "__" + hours + ":" + minutes + ":" + seconds);
}

// const mainPath = `${folderCSV}/${MAIN_IMAGES}`;

// let finishFileData = [];

// const openMainFileStream = (resolve) => {
//     return fs.createReadStream(mainPath)
//         .pipe(csv())
//         .on('data', (row) => {
//             const token_id = row['token_id'];
//             const main_json_url = row['json_url'];
//             finishFileData.push(main_json_url);
//         })
//         .on('end', () => {
//             console.log('CSV file successfully processed', mainPath);
//             return resolve()
//         });
// }

// const appendToFile = async (content) => {
//     try {
//         fs.appendFile(UPLOAD_IMAGES_INFO_LOG, content, function(){});
//     } catch (e) {
//         console.error(e)
//         let err_msg = `- ${content}; ${e}`
//         fs.appendFile(UPLOAD_IMAGES_ERROR_LOG, err_msg, function(){});
//     }
// }

// /*
// Installes main images JSON hashes to the NFT smart contract for sold tokens
// */
// async function setJSONtoSC() {
//     const Token = await ethers.getContractFactory("NFT");
//     const _nft = await Token.attach(nft);
//     const _shift = await _nft.shift.call()
//     const _soldAmountOfTokens = await _nft.lastTokenBeforeReveal.call()
//     let shift = parseInt(_shift);
//     let soldAmountOfTokens = parseInt(_soldAmountOfTokens);
//     let receipt, msg;
//     let index_start = 0;
//     let shiftedID, tokenId;
//     for (let i = index_start; i < soldAmountOfTokens; i++) {
//         if( i + shift <= amountOfTokens) {
//             shiftedID = i + shift;
//         } else {
//             shiftedID = shift + i - amountOfTokens;
//         }
//         tokenId = i + 1;
//         console.log("shiftedID", shiftedID);
//         console.log("tokenId", tokenId);
//         console.log("CID", finishFileData[shiftedID - 1], "\n");
//         receipt = await _nft.setHash(tokenId, finishFileData[shiftedID - 1]);
//         msg = `Token: ${tokenId}, tx: ${receipt.hash}\n`
//         await appendToFile(msg);
//     }
// }

async function checkConfig() {
	// check that network is correct and that config file has one of executed network
	if(networks[network].contract == undefined) {
		await new Promise(() => {
	        throw `Network ${network} doesn't exist. Check the name network in the scripts/config.json file`;
	    }).catch(error => console.error(error))
	}
	// check that the addresses in config file is a contract address
	// new Promise(() => {
	// 	web3.eth.getCode(networks[network].contract).then(result => {
	// 		if(result == "0x") {
	// 	        throw `The address in config.json file should be a contract in the ${network} network`;
	// 	    }
	// 	}).catch(error => console.error(error))
	// });
}


function getTransactionCount() {
	/* 
	execute error if the contract doesn't have any transactions (exept contract creation),
	otherwise store all transactions of the contract to json file.
	As standart API returns no more then 10000 tx, but we will have contracts with more tx
	then it needs to use pagination in API calls
	*/

	return new Promise((resolve, reject) => {
		web3.eth.getTransactionCount(networks[network].contract).then(result => {
			if(result == 0) {
		        reject(`The contract in config.json file should have more than one tx in the ${network} network`);
		    } else {
		    	resolve(result);
		    }
		})
	});
}

function getURL(_pageNumber, _offset) {
	const data = {
		module: 'account',
		action: 'txlist',
		address: networks[network].contract,
		startblock: startblock,
		endblock: endblock,
		page: _pageNumber,
		offset: _offset,
		sort: "asc",
		apikey: networks[network].apiKey
	}
	const qs = querystring.stringify(data);
	return `${networks[network].api}?${qs}`;
}

async function loadAllTransactions() {
	/* 
	execute error if the contract doesn't have any transactions (exept contract creation),
	otherwise store all transactions of the contract to json file.
	As standart API returns no more then 10000 tx, but we will have contracts with more tx
	then it needs to use pagination in API calls
	*/
	let count = 0;
	let page = 1;
	let offset = 10000; // etherscan won't return more that this value
	const countPromise = getTransactionCount();
	await countPromise.then(res => {count = res;}
	, console.log);

	let url, txs;

	if(count != 0)  {
		while(1) {
			url = getURL(page, parseInt(offset/page));
    		txs = await fetch(url).then(response => response.json());
    		if (txs.status == 0) {console.log("hmm"); break; }// try to find the last page
    		// increase pages till finish to parse data
    		page++;
    		// filter tx by datetime
    		// console.log("b",txs.result);
    		// txs = await filterByDateTimes(txs.result);
    		// console.log("a",txs.length,"\n");
    		// transactions.push(...txs.result);
    		mySet.add(...txs.result);
    	}
	}
	// console.log(transactions.length);
	console.log(mySet.size);
}

function _isBetweenDateTimes(transaction) {
	return 
		transaction.timeStamp <= networks[network].endDate && 
		transaction.timeStamp >= networks[network].startDate;
}

function filterByDateTimes(transactions) {
	return transactions.filter(_isBetweenDateTimes);
}


const main = async () => {
	// check the config.json file data
    await checkConfig();
    // get all tx by address, tx store in 'transactions' variable
    await loadAllTransactions(console.log, console.log);
    
}


(async function () {
    await main();
}());
