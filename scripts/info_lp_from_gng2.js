/* This script is created for getting all wallets that have positive stakingToken balances
	in the SC(smart contract) from start date of the smart contract (when the SC)
	was created till certain date (T).

	This script save to the file only positive balances during this (T) period of time.

	As the result it stores a .csv file in the results/ directory for each script executing to the SC.
*/

const hre = require("hardhat");
const path = require("path");
const fs = require("fs");
const { resolve } = require("path");
const csv = require('csv-parser');
const { networks, startblock, endblock }  = require('./config.json');
const https = require('https');
const querystring = require('querystring');
const fetch = require("node-fetch");
const InputDataDecoder = require('ethereum-input-data-decoder');

const decoder = new InputDataDecoder(`${__dirname}/abi/abi.json`);
const network = hre.hardhatArguments.network;
const folderCSV = path.join(__dirname, './output');
const inputFolderCSV = path.join(__dirname, './input');
const folderLOGS = path.join(__dirname, './logs');
const CURRENT_DATE_TIME = getTimeName();
const ERROR_LOG = `${folderLOGS}/error_${CURRENT_DATE_TIME}.log`;
const MAIN_INPUT_FILE_NAME = `${inputFolderCSV}/export-${networks[network].contract}.csv`;
const MAIN_OUTPUT_FILE_NAME = `${folderCSV}/${network}_wallets_info_${CURRENT_DATE_TIME}.csv`;

let transactions = [];
let wallets = {};

function getTimeName() {
	let date_ob = new Date();

	// current date
	// adjust 0 before single digit date
	let date = ("0" + date_ob.getDate()).slice(-2);

	// current month
	let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

	// current year
	let year = date_ob.getFullYear();

	// current hours
	let hours = date_ob.getHours();

	// current minutes
	let minutes = date_ob.getMinutes();

	// current seconds
	let seconds = date_ob.getSeconds();

	// returns YYYY-MM-DD_HH:MM:SS format
	return (year + "-" + month + "-" + date + "__" + hours + ":" + minutes + ":" + seconds);
}

// const mainPath = `${folderCSV}/${MAIN_IMAGES}`;

const openMainFileStream = (resolve) => {
    return fs.createReadStream(MAIN_INPUT_FILE_NAME)
        .pipe(csv())
        .on('data', (row) => {
            const tx = row['Txhash'];
            const status = row['Status'];
            // we don't need to have reverted transactions
            if(status == '') {
            	transactions.push(tx);
            }
        })
        .on('end', () => {
            console.log('CSV file successfully processed', MAIN_INPUT_FILE_NAME);
            return resolve()
        });
}

async function updateWallets() {
	let _txs = transactions.slice(0, 1000);
	// todo: make it till end of file
	for(i = 0; i < _txs.length; i++) {
		new Promise(() => {
			web3.eth.getTransaction(_txs[i]).then(result => {
				let inputTx = decoder.decodeData(result.input);
				if(inputTx.method == 'deposit' ||
					inputTx.method == 'withdraw') {
					let inputData = inputTx.inputs.toString().split(',');
					let pid = inputData[0];
					let amount = inputData[1];
					if(pid == 1 && amount != 0) {
						let key = `${result.from}`;
						let operation = inputTx.method == 'deposit'? '+' : '-';

						if(wallets.hasOwnProperty(key)) {
							if(operation == '+') {
								wallets[key] = wallets[key] + amount;
							} else {
								wallets[key] = wallets[key] - amount;
							}
						}
						wallets[key] = amount;
					}
				} else if(inputTx.method == 'emergencyWithdraw') {
					// todo
					console.log(inputTx);
					return;
				}
			})
		});
	}
}

const appendToFile = async (content) => {
    try {
        fs.appendFile(UPLOAD_IMAGES_INFO_LOG, content, function(){});
    } catch (e) {
        console.error(e)
        let err_msg = `- ${content}; ${e}`
        fs.appendFile(UPLOAD_IMAGES_ERROR_LOG, err_msg, function(){});
    }
}

async function checkConfig() {
	// check that network is correct and that config file has one of executed network
	if(networks[network].contract == undefined) {
		await new Promise(() => {
	        throw `Network ${network} doesn't exist. Check the name network in the scripts/config.json file`;
	    }).catch(error => console.error(error))
	}
	// check that the addresses in config file is a contract address
	new Promise(() => {
		web3.eth.getCode(networks[network].contract).then(result => {
			if(result == "0x") {
		        throw `The address in config.json file should be a contract in the ${network} network`;
		    }
		}).catch(error => console.error(error))
	});
}


const main = async () => {
	// check the config.json file data
    await checkConfig();
    // get all tx by address, tx store in 'transactions' variable
    // await loadAllTransactions(console.log, console.log);
    await new Promise((resolve, reject) => {
        openMainFileStream(resolve);
        }).then((r) => {
            console.log(r);
        }).catch(e => {
            console.log(e);
            return reject();
        })
    // console.log(transactions.length);
    await updateWallets();
    console.log(wallets);
}


(async function () {
    await main();
}());
