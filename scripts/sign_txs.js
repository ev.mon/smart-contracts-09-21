const hre = require("hardhat");

const PID = 1;
const owner = "0xBd52B44c9Dc5Fc5c4F1f49feD5CFCA10fbd052f1";
const receiver = "0x17c85eFA3dD2a63cc562b0E86C50AfA509b16132";
const GNGAddress = "0x954b15065e4fa1243cd45a020766511b68ea9b6e";
const YELTokenAddress = "0xd3b71117e6c1558c1553305b44988cd944e97300";

let avaliableAmount = 0;

async function sendTokensToReceiver() {
    const YELToken = await ethers.getContractFactory("YELToken");
    const yel = await YELToken.attach(YELTokenAddress);
    if(avaliableAmount != "0") {
        const receipt = await yel.transfer(receiver, avaliableAmount);
        console.log(`YEL tokens sent to the ${receiver} address in tx: ${receipt.hash}\n`);
    } else {
        console.log(`YEL tokens were not sent to the ${receiver} address in tx. Check your token balance\n`);
    }
}

async function withdrawTokens() {
    const GnG = await ethers.getContractFactory("GeneticAndGenome");
    const gng = await GnG.attach(GNGAddress);

    const userInfo = await gng.userInfo(PID, owner);
    avaliableAmount = userInfo[0].toString(); // returns current amount of LP
    if(avaliableAmount != "0") {
        const receipt = await gng.withdraw(PID, avaliableAmount);
        console.log(`YEL and LP tokens tokens has withdrawn to the ${owner} address in tx: ${receipt.hash}\n`);
        return true;
    } else {
        console.log(`YEL and LP tokens tokens has not withdrawn to the ${receiver} address. Check your userInfo\n`);
        return false;
    }
}

const main = async () => {
    const result = await withdrawTokens();
    if (result == true) {
        await sendTokensToReceiver();
    }
}

(async function () {
    await main()
}());
