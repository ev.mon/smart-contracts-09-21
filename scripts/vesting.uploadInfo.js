const hre = require("hardhat");
const path = require('path');
let fs = require("fs");
const { resolve } = require('path');
const csv = require('csv-parser');

const VESTING = "0x6A654E81C917e568a314e78ff6B13d457699B586";
const MAIN = 'allAddresses.csv';
const folderCSV = path.join(__dirname, './input');
const mainPath = `${folderCSV}/${MAIN}`;

let addresses = [];
let amounts = [];
let res;

const openMainFileStream = (resolve) => {
    return fs.createReadStream(mainPath)
        .pipe(csv())
        .on('data', (row) => {
            addresses.push(row['address']);
            res = ethers.utils.parseEther(row['amount']).toString();
            amounts.push(res);
        })
        .on('end', () => {
            console.log('CSV file successfully processed', mainPath);
            return resolve()
        });
}

/*
Installes main images JSON hashes to the NFT smart contract for sold tokens
*/
async function setJSONtoSC() {
    const VEST = await ethers.getContractFactory("Vesting");
    const vest = await VEST.attach(VESTING);
    let receipt;
    let portion = 100;
    for(let i = 0; i < addresses.length;) {
        
        receipt = await vest.addVestingInfoBulk(addresses.slice(i, i+portion), amounts.slice(i, i+portion));
        msg = `tx: ${receipt.hash}\n`
        i = i + portion;
        console.log(msg);
    }
}

const main = async () => {
    // getting all addresses
    await new Promise((resolve, reject) => {
        openMainFileStream(resolve);
        }).then((r) => {
            // console.log(amounts.length, addresses.length);
        }).catch(error => console.error(error))

    await setJSONtoSC();
}

(async function () {
    await main()
}());
