var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
chai.use(require("chai-bignumber")());
const {
    expectRevert
} = require("@openzeppelin/test-helpers");
// ethers - The ethers variable is available in the global scope
const {
    BigNumber
} = require("ethers");
const { set, reset } = require('mockdate');
const GVABI = require('../scripts/abi/GrowthVault.json');
const StratABI = require('../scripts/abi/StratABI.json');
// const { smockit, smoddit } = require("@eth-optimism/smock");
const {deployMockContract} =  require('@ethereum-waffle/mock-contract');


async function freshGrowthVault(contract, token_address, name, symbol) {
    let contr = await contract.deploy(name, symbol, token_address);
    await contr.deployed();
    return contr;
}

async function freshUniswapV2Pair(contract) {
    let contr = await contract.deploy();
    await contr.deployed();
    return contr;
}


describe("GrowthVault", function() {
    let owner, government, strat, addr1, addr2, addr3, addr4, addr5, addr6, addr7;
    let provider = new ethers.providers.JsonRpcProvider();
    let GrowthVault, Strat, ElevenGrowthVault, PankakePair;
    let contractAsOwner, contractAsAddr1, contractAsAddr4;
    const zeroAddress = "0x0000000000000000000000000000000000000000";

    beforeEach(async function() {
        [owner, government, addr2, addr3, addr4, addr5, addr6, addr7, _] = await ethers.getSigners();

        GrowthVault = await ethers.getContractFactory("GrowthVault");
        UniswapV2Pair = await ethers.getContractFactory("UniswapV2Pair");
        Strat = await ethers.getContractFactory("Strat");
        MasterChef = await ethers.getContractFactory("MasterChef");

        tokenPair = await UniswapV2Pair.deploy();
        await tokenPair.deployed()

        const masterChef = await MasterChef.deploy(
            tokenPair.address, addr3.address, addr4.address, 1000, 1);
        await masterChef.deployed()

        growthVault = await GrowthVault.deploy("GV", "GV", tokenPair.address);
        await growthVault.deployed()

        strat = await Strat.deploy(tokenPair.address, growthVault.address, 0, masterChef.address);
        await strat.deployed()

        cAsOwner = growthVault.connect(owner);
        tAsOwner = tokenPair.connect(owner);

        cAsGovernment = growthVault.connect(government);
        cAsAddr1 = growthVault.connect(addr1);
        cAsAddr2 = growthVault.connect(addr2);
        cAsAddr3 = growthVault.connect(addr3);
    });

    /* describe("Base functionality", function() {
        it("1. Should check base values for a GrowthVault and tokenPair contracts.", async function() {
            expect(await tokenPair.name()).to.equal("Spooky LP");
            expect(await tokenPair.symbol()).to.equal("spLP");

            expect(await growthVault.name()).to.equal("GV");
            expect(await growthVault.symbol()).to.equal("GV");
            expect(await growthVault.token()).to.equal(tokenPair.address);
        });

        it("2. Should check pausable is true for a GrowthVault contract.", async function() {
            expect((await growthVault.paused()).toString()).to.equal("true");
        });

        it("3. Should check pausable is true for two versions of GrowthVault contracts.", async function() {
            expect((await growthVault.paused()).toString()).to.equal("true");
            const _token = await freshUniswapV2Pair(UniswapV2Pair);
            const _contr = await freshGrowthVault(GrowthVault, _token.address, "Test", "TestS");
            expect((await _contr.paused()).toString()).to.equal("true");
        });
    });

    describe("Ownership", function() {
        it("1. Should check that owner can change government address", async function() {
            expect(await growthVault.government()).to.equal(zeroAddress);
            await cAsOwner.setGovernment(government.address);
            expect(await growthVault.government()).to.equal(government.address);
        });

        it("2. Should check that simple user can not change government address", async function() {
            expect(await growthVault.government()).to.equal(zeroAddress);
            await expectRevert(
                cAsAddr3.setGovernment(government.address),
                "Ownable: caller is not the owner"
            );
            expect(await growthVault.government()).to.equal(zeroAddress);
        });

        it("3. Should check that owner and government can unpause the contract", async function() {
            expect((await growthVault.paused()).toString()).to.equal("true");
            await cAsOwner.unpause();
            expect((await growthVault.paused()).toString()).to.equal("false");
            await cAsOwner.pause();
            expect((await growthVault.paused()).toString()).to.equal("true");

            await cAsOwner.setGovernment(government.address);

            await cAsGovernment.unpause();
            expect((await growthVault.paused()).toString()).to.equal("false");
            await cAsGovernment.pause();
            expect((await growthVault.paused()).toString()).to.equal("true");
        });

        it("4. Should check that simple user can not execute migrate", async function() {
            expect((await growthVault.paused()).toString()).to.equal("true");
            await cAsOwner.unpause();
            expect((await growthVault.paused()).toString()).to.equal("false");

            expectRevert(
                cAsAddr3.migrate(addr3.address, addr4.address),
                "Ownable: caller is not the owner"
            );
            expect((await growthVault.paused()).toString()).to.equal("false");
        });
    });

    describe("Pausable", function() {
        it("1. Should check that migrate function is not avaliable when contract is paused", async function() {
            await expectRevert(
                cAsOwner.migrate(addr3.address, addr4.address),
                "Pausable: paused"
            );
        });

        it("2. Should check that deposit function is avaliable when contract is not paused", async function() {
            const MockStrat = await deployMockContract(owner, StratABI);
            await cAsOwner.unpause();

            await MockStrat.mock.balanceOf.returns(50);
            await MockStrat.mock.retireStrat.returns(0);
            await MockStrat.mock.deposit.returns(0);
            await cAsOwner.setStrategy(MockStrat.address);
            await cAsOwner.connect(addr2).deposit(0);
        });

        it("3. Should check that withdraw function is not avaliable when contract is paused", async function() {
            await expectRevert(
                cAsOwner.connect(addr2).withdraw(30),
                "Pausable: paused"
            );
        });

        it("4. Should check that withdrawAll function is not avaliable when contract is paused", async function() {
            await expectRevert(
                cAsAddr3.withdrawAll(),
                "Pausable: paused"
            );
        });

        it("5. Should check that earn function is not avaliable when contract is paused", async function() {
            await expectRevert(
                cAsAddr3.earn(),
                "Pausable: paused"
            );
        });

        it("6. Should check that deposit function is not avaliable when contract is paused", async function() {
            const MockStrat = await deployMockContract(owner, StratABI);

            await MockStrat.mock.balanceOf.returns(50);
            await MockStrat.mock.retireStrat.returns(0);
            await MockStrat.mock.deposit.returns(0);
            await cAsOwner.setStrategy(MockStrat.address);

            await expectRevert(
                cAsOwner.connect(addr2).deposit(10),
                "Pausable: paused"
            );
        });

        it("7. Should check that depositAll function is not avaliable when contract is paused", async function() {
            const MockStrat = await deployMockContract(owner, StratABI);

            await MockStrat.mock.balanceOf.returns(50);
            await MockStrat.mock.retireStrat.returns(0);
            await MockStrat.mock.deposit.returns(0);
            await cAsOwner.setStrategy(MockStrat.address);

            await expectRevert(
                cAsOwner.connect(addr2).depositAll(),
                "Pausable: paused"
            );
        });

        it("8. Should check that depositAll function is avaliable when contract is not paused", async function() {
            await cAsOwner.unpause();

            const MockStrat = await deployMockContract(owner, StratABI);

            await MockStrat.mock.balanceOf.returns(50);
            await MockStrat.mock.retireStrat.returns(0);
            await MockStrat.mock.deposit.returns(0);
            await cAsOwner.setStrategy(MockStrat.address);

            await cAsAddr3.depositAll();
        });

        // it("9. Should check that withdraw function is avaliable when contract is not paused", async function() {
        //     const MockStrat = await deployMockContract(owner, StratABI);
        //     await cAsOwner.unpause();

        //     await MockStrat.mock.balanceOf.returns(50);
        //     await MockStrat.mock.retireStrat.returns(0);
        //     await MockStrat.mock.deposit.returns(0);

        //     await cAsOwner.setStrategy(MockStrat.address);
        //     await cAsOwner.connect(addr2).deposit(0);

            // await cAsAddr3.withdraw(0);
        // });

        // it("10. Should check that withdrawAll function is avaliable when contract is not paused", async function() {
        //     await tAsOwner.mint(addr3.address, 1000);
        //     await cAsOwner.unpause();
        //     await cAsAddr3.deposit(100);
        //     await cAsAddr3.withdrawAll();
        // });

        it("11. Should check that withdrawTokensFee function is avaliable when contract is not paused", async function() {
            await cAsOwner.unpause();
            await cAsOwner.withdrawTokensFee();
        });

        it("12. Should check that earn function is avaliable when contract is not paused", async function() {
            await cAsOwner.unpause();

            const MockStrat = await deployMockContract(owner, StratABI);

            await MockStrat.mock.deposit.returns(0);
            await cAsOwner.setStrategy(MockStrat.address);

            await cAsAddr3.earn();
        });

        it("13. Should check that withdrawTokensFee function is avaliable when contract is paused", async function() {
            await cAsOwner.withdrawTokensFee();
        });
    });**/

    describe("Migration", function() {
        it("1. Should migrate tokens by owner and pause the contract", async function() {
            await cAsOwner.setStrategy(strat.address);

            const _token = await freshUniswapV2Pair(UniswapV2Pair);
            const newContract = await freshGrowthVault(GrowthVault, _token.address, "Test2", "TestS2");

            await cAsOwner.unpause();
            await cAsOwner.migrate(newContract.address, _token.address);
        });

        // it("2. Should not migrate tokens if the contract address is 0", async function() {
        //     await cAsOwner.setStrategy(strat.address);

        //     const _token = await freshUniswapV2Pair(UniswapV2Pair);

        //     await cAsOwner.unpause();
        //     await expectRevert(
        //         cAsOwner.migrate(zeroAddress, _token.address),
        //         "The contract can not be a zero address"
        //     );
        // });

        // it("3. Should not migrate tokens if the token address is 0", async function() {
        //     await cAsOwner.setStrategy(strat.address);

        //     const _token = await freshUniswapV2Pair(UniswapV2Pair);
        //     const newContract = await freshGrowthVault(GrowthVault, _token.address, "Test2", "TestS2");

        //     await cAsOwner.unpause();
        //     await expectRevert(
        //         cAsOwner.migrate(newContract.address, zeroAddress),
        //         "The token can not be a zero address"
        //     );
        // });

        // it("4. Should migrate tokens by government and pause the contract", async function() {
        //     await cAsOwner.setStrategy(strat.address);

        //     const _token = await freshUniswapV2Pair(UniswapV2Pair);
        //     const newContract = await freshGrowthVault(GrowthVault, _token.address, "Test2", "TestS2");
        //     await cAsOwner.unpause();
        //     await cAsOwner.setGovernment(government.address);
        //     await cAsGovernment.migrate(newContract.address, _token.address);
        // });
    });

    // describe("Withdraw tokens fee", function() {
    //     it("1. Should withdraw tokens fee from the contract by the owner", async function() {
    //         await tAsOwner.mint(addr3.address, 1000);
    //         await cAsAddr3.deposit(100);
    //         await cAsAddr3.withdraw(100);
    //         expect((await cAsAddr3.totalAmountTokensFee()).toString()).to.equal("1");
    //         await cAsOwner.withdrawTokensFee();
    //         expect((await cAsAddr3.totalAmountTokensFee()).toString()).to.equal("0");
    //     });

    //     it("2. Should not withdraw tokens fee from the contract if the caller is not the owner", async function() {
    //         await expectRevert(
    //             cAsAddr3.withdrawTokensFee(),
    //             "Ownable: caller is not the owner"
    //         );
    //     });

    //     it("3. Should not withdraw tokens if totalAmountTokensFee is not equal or less then current balance", async function() {
    //         await tAsOwner.mint(addr3.address, 1000);
    //         await tAsOwner.mint(addr2.address, 1000);
    //         await cAsAddr3.deposit(100);
    //         await cAsAddr3.withdraw(100);
            
    //         expect((await cAsAddr3.totalAmountTokensFee()).toString()).to.equal("1");
    //         await cAsAddr2.earn();
    //         expect((await cAsAddr3.totalAmountTokensFee()).toString()).to.equal("0");
    //         await expectRevert(
    //             await cAsOwner.withdrawTokensFee(),
    //             "GrowthVault: the token balance of the contract should be equal or more then totalAmountTokensFee"
    //         );
    //     });
    // });

    // describe("Deposit", function() {
    //     it("1. Should deposit all existing tokens from the user", async function() {
    //         await tAsOwner.mint(addr3.address, 1000);
    //         expect((await tAsOwner.balanceOf(addr3.address)).toString()).to.equal("1000");
    //         await cAsAddr3.depositAll();
    //         expect((await tAsOwner.balanceOf(addr3.address)).toString()).to.equal("0");
    //     });

    //     it("2. Should deposit some amount of existing tokens from the user", async function() {
    //         await tAsOwner.mint(addr3.address, 1000);
    //         expect((await tAsOwner.balanceOf(addr3.address)).toString()).to.equal("1000");
    //         await cAsAddr3.deposit(100);
    //         expect((await tAsOwner.balanceOf(addr3.address)).toString()).to.equal("900");
    //     });
    // });
});