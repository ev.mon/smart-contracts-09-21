var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
chai.use(require("chai-bignumber")());
const {
    expectRevert
} = require("@openzeppelin/test-helpers");
// ethers - The ethers variable is available in the global scope
const {
    BigNumber
} = require("ethers");
const { set, reset } = require('mockdate');


describe("NFT", function() {
    let owner, addr1, addr2, addr3, addr4, addr5, nctAcc, nctAddress, rewardAddress;
    let provider = new ethers.providers.JsonRpcProvider();
    let NFT, contract, cap, totalCap, contractAsOwner, contractAsAddr1, contractAsAddr2;

    const _date = new Date();
    const _dateNext = new Date();
    const _dateEnd = new Date();
    const dateNow = (_date.setDate(_date.getDate()) / 1000) | 0;
    const dateInThePast = (_date.setDate(_date.getDate() - 10) / 1000) | 0;
    const dateInThePastForTimer = (_date.setDate(_date.getDate() - 10));
    const nextDate = (_dateNext.setDate(_dateNext.getDate() + 10) / 1000) | 0;
    const endDate = (_dateEnd.setDate(_dateEnd.getDate() + 20) / 1000) | 0;
    const afterEndDate = (_dateEnd.setDate(_dateEnd.getDate() + 21));

    let defaultJson = 'https://gateway.pinata.cloud/ipfs/QmWBF9Yvdx7DwfjLEMrFZ1cHda1puemwGnx4JBS82rBXPM';

    beforeEach(async function() {
        [owner, addr1, addr2, addr3, addr4, addr5, nctAcc, reward, _] = await ethers.getSigners();
        cap = 3;

        NFT = await ethers.getContractFactory("NFT");
        contract = await NFT.deploy("Test NFT", "TNFT", cap);
        await contract.deployed()
        contractAsOwner = contract.connect(owner);
        contractAsAddr1 = contract.connect(addr1);
        contractAsAddr2 = contract.connect(addr2);
    });

    describe("Getting base parameters of contracts", function() {
        it("1. Should deploy contract with constrgbt parameters", async function() {
            expect(await contract.name()).to.equal("Test NFT");
            expect(await contract.symbol()).to.equal("TNFT");
        });

        it("2. Should check the base URI", async function() {
            let baseURI = "https://example.com/"
            let contractAsOwner = contract.connect(owner);

            expect(await contractAsOwner.baseURIPrefix()).to.equal("");

            await contractAsOwner.setBaseURI(baseURI);

            expect(await contractAsOwner.baseURIPrefix()).to.equal(baseURI);
        });
    });

    describe("Setting URI for tokens", function() {
        it("1. Should return defaultJson if base uri was not set before", async function() {
            await contractAsOwner.safeMint(addr1.address);
            await contractAsOwner.safeMint(addr1.address);
            await contractAsOwner.safeMint(addr1.address);

            expect(await contractAsAddr2.tokenURI(1)).to.be.equal(defaultJson);
            expect(await contractAsAddr2.tokenURI(2)).to.be.equal(defaultJson);
            expect(await contractAsAddr2.tokenURI(3)).to.be.equal(defaultJson);
        });

        it("2. Should revert when a simple user(not owner) tries to set base URI for tokens", async function() {
            let baseURI = "https://example.com/"
            let baseURIEmpty = ""

            expect(await contractAsAddr2.baseURIPrefix()).to.equal(baseURIEmpty);
            await expectRevert(contractAsAddr2.setBaseURI(baseURI), "Ownable: caller is not the owner");
            expect(await contractAsAddr2.baseURIPrefix()).to.equal(baseURIEmpty);
        });

        it("3. Should return sutaible uri for the token id basic uri already set", async function() {
            await contractAsOwner.safeMint(addr1.address);
            await contractAsOwner.safeMint(addr1.address);
            await contractAsOwner.safeMint(addr1.address);

            expect(await contractAsAddr2.tokenURI(1)).to.be.equal(defaultJson);
            expect(await contractAsAddr2.tokenURI(2)).to.be.equal(defaultJson);
            expect(await contractAsAddr2.tokenURI(3)).to.be.equal(defaultJson);

            await contractAsOwner.setBaseURI("https://example.com/");

            expect(await contract.tokenURI(1)).to.be.equal("https://example.com/1");
            expect(await contract.tokenURI(2)).to.be.equal("https://example.com/2");
            expect(await contract.tokenURI(3)).to.be.equal("https://example.com/3");
        });
    });

    it("- Should revert transfer tokens from a user who has no tokens", async function() {
        await contractAsOwner.safeMint(addr1.address);
        await contractAsOwner.safeMint(addr1.address);
        await contractAsOwner.safeMint(addr1.address);
        expect((await contractAsOwner.balanceOf(addr1.address)).toString()).to.equal(cap.toString());
        expect((await contract.balanceOf(addr2.address)).toString()).to.equal("0");

        // uses this calling method because of a weird "TypeError: contractAsAddr1.safeTransferFrom is not a function" error
        // for calling await contractAsAddr1.safeTransferFrom(addr1.address, addr2.address, 1);
        await expectRevert(
            contractAsAddr2['safeTransferFrom(address,address,uint256)'](addr2.address, addr1.address, 1),
            "ERC721: transfer caller is not owner nor approved"
        );

        expect((await contractAsOwner.balanceOf(addr1.address)).toString()).to.equal(cap.toString());
        expect((await contract.balanceOf(addr2.address)).toString()).to.equal("0");

        expect((await contract.ownerOf(1)).toString()).to.equal(addr1.address);
        expect((await contract.ownerOf(2)).toString()).to.equal(addr1.address);
        expect((await contract.ownerOf(3)).toString()).to.equal(addr1.address);
    });

    it("- Should check that only tokenSale contract and owner of contracts can mint tokens.", async function() {
        let _totalCap = 5;
        let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
        await _tokenContract.deployed();

        let _tokenAsOwner = _tokenContract.connect(owner);
        let _tokenAsAddr3 = _tokenContract.connect(addr3);

        await _tokenAsOwner.safeMint(addr1.address);

        await expectRevert(
            _tokenAsAddr3.safeMint(addr3.address),
            "Ownable: caller is not the owner or the sale agent");

        let TokenSale = await ethers.getContractFactory("TokenSale");
        let tokenSale = await TokenSale.deploy(_tokenContract.address, dateInThePast);
        await tokenSale.deployed()

        // sending Eth to the contract
        await expectRevert(
            addr3.sendTransaction({
            to: tokenSale.address,
            value: ethers.utils.parseEther("0.01")
        }), "Ownable: caller is not the owner or the sale agent");

        await _tokenAsOwner.setSaleAgent(tokenSale.address);

        // sending Eth to the contract
        await addr3.sendTransaction({
            to: tokenSale.address,
            value: ethers.utils.parseEther("0.01")
        })

    });

    it("- Should check that can not mint the token with id that is more then totalCap.", async function() {
        let _totalCap = 5;
        let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
        await _tokenContract.deployed();

        let _tokenAsOwner = _tokenContract.connect(owner);
        await _tokenAsOwner.safeMint(addr3.address);
        await _tokenAsOwner.safeMint(addr3.address);
        await _tokenAsOwner.safeMint(addr3.address);
        await _tokenAsOwner.safeMint(addr3.address);
        await _tokenAsOwner.safeMint(addr3.address);

        await expectRevert(
            _tokenAsOwner.safeMint(addr3.address),
            "NFT: Can not mint tokens anymore"
        );
    });

    it("- Should return minted token list.", async function() {
        let _totalCap = 5;
        let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
        await _tokenContract.deployed();

        let _tokenAsOwner = _tokenContract.connect(owner);
        expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('0');
        await _tokenAsOwner.safeMint(addr1.address);
        await _tokenAsOwner.safeMint(addr2.address);
        await _tokenAsOwner.safeMint(addr1.address);
        expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('3');
        expect((await _tokenAsOwner.getMintedTokens()).toString()).to.equal("1,2,3");
    });

    it("- Should check that owner of contracts can mint tokens.", async function() {
        let _totalCap = 5;
        let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
        await _tokenContract.deployed();

        let _tokenAsOwner = _tokenContract.connect(owner);
        expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('0');
        await _tokenAsOwner.safeMint(addr1.address);
        expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('1');
    });

    it("- Should check that one of token exists and anotherone does not exist.", async function() {
        let _totalCap = 5;
        let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
        await _tokenContract.deployed();

        let _tokenAsOwner = _tokenContract.connect(owner);

        await _tokenAsOwner.safeMint(addr1.address);
        expect((await _tokenAsOwner.isMinted(1)).toString()).to.equal('true');
        expect((await _tokenAsOwner.isMinted(2)).toString()).to.equal('false');
    });
});
