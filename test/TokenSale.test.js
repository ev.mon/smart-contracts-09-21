var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
chai.use(require("chai-bignumber")());
const {
    expectRevert
} = require("@openzeppelin/test-helpers");
// ethers - The ethers variable is available in the global scope
const {
    BigNumber
} = require("ethers");
const { set, reset } = require('mockdate');


describe("TokenSale", function() {
    let owner, addr1, addr2, addr3, addr4, addr5;
    let provider = new ethers.providers.JsonRpcProvider();
    let TokenSale, NFT, tokenSale, tokenContract;
    let totalCap, cap;
    let contractAsOwner, contractAsAddr1, contractAsAddr4;

    const _date = new Date();
    const _date1 = new Date();
    const _dateNext = new Date();
    const _dateEnd = new Date();
    
    const dateNow = (_date.setDate(_date.getDate()) / 1000) | 0;
    const dateInThePast = (_date.setDate(_date.getDate() - 10) / 1000) | 0;
    const dateInThePastForTimer = (_date.setDate(_date.getDate() - 10));
    const nextDate = (_dateNext.setDate(_dateNext.getDate() + 10) / 1000) | 0;
    const endDate = (_dateEnd.setDate(_dateEnd.getDate() + 20) / 1000) | 0;
    const afterEndDate = (_dateEnd.setDate(_dateEnd.getDate() + 21));

    beforeEach(async function() {
        [owner, addr1, addr2, addr3, addr4, addr5, addr6, reward, _] = await ethers.getSigners();

        NFT = await ethers.getContractFactory("NFT");
        TokenSale = await ethers.getContractFactory("TokenSale");

        totalCap = cap = 3;
        tokenContract = await NFT.deploy("Test NFT", "TNFT", totalCap);
        await tokenContract.deployed()

        tokenSale = await TokenSale.deploy(tokenContract.address, dateInThePast);
        await tokenSale.deployed()

        saleAsOwner = tokenSale.connect(owner);
        tokenAsOwner = tokenContract.connect(owner);
        contractAsAddr1 = tokenSale.connect(addr1);
        tokenAsAddr4 = tokenContract.connect(addr4);
        contractAsAddr4 = tokenSale.connect(addr4);
    });

    describe("Sale stages", function() {
        it("1. Should check base values for a token and token sale contracts.", async function() {
            expect(await tokenContract.name()).to.equal("Test NFT");
            expect(await tokenContract.symbol()).to.equal("TNFT");
            expect((await tokenSale.price()).toString()).to.equal('10000000000000000');
        });

        it("2. Should check that user can't sent 0 ETH to the contract.", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);
            // sending Eth to the contract
            await expectRevert(addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0")
            }), "TokenSale: ETH value is not enough for token buying");
            // checking balance for the contract and a buyer
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("0");
        });

        it("3. Should check that user can buy one tokens while sending ETH to the contract.", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);
            // issuing tokens to the contract
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("0");

            // sending Eth to the contract
            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            // checking balance for the contract and a buyer
            expect((await tokenAsOwner.balanceOf(tokenSale.address)).toString()).to.equal("0");
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");
        })

        it("4. Should check that user can't buy tokens if he sent less value of ETH.", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);
            // sending Eth to the contract
            await expectRevert(addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.002")
            }), "TokenSale: ETH value is not enough for token buying");
            // checking balance for the contract and a buyer
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("0");
        });

        it("5. Should check that user can buy tokens if he sent more value of ETH.", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);
            // sending Eth to the contract
            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.021")
            });
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("2");

            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.019")
            });
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("3");
        });

        it("6. Should check that when a user can buy tokens, his ETH will be transferred to the contract.", async function() {
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", totalCap);
            await _tokenContract.deployed()
            let _tokenSale = await TokenSale.deploy(_tokenContract.address, dateInThePast);
            await _tokenSale.deployed()
            let _tokenAsOwner = _tokenContract.connect(owner);

            // set sale agent for the token contract
            await _tokenAsOwner.setSaleAgent(_tokenSale.address);
            // sending Eth to the contract
            await addr4.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            });
            // checking ETH balance for a buyer
            expect((await _tokenAsOwner.balanceOf(addr4.address)).toString()).to.equal("1");

            expect((await ethers.provider.getBalance(_tokenSale.address)).toString()).to.equal("10000000000000000");
            // 9999,9... ETH
            expect((await ethers.provider.getBalance(addr4.address)).toString()).not.to.equal("10000000000000000000000");
        });

        it("7. Should check that user can't buy tokens if token sale hasn't started yet.", async function() {
            // start time and end time are set in the future
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", totalCap);
            await _tokenContract.deployed()

            let _tokenSale = await TokenSale.deploy(_tokenContract.address, nextDate);
            await _tokenSale.deployed()

            let _tokenAsOwner = _tokenContract.connect(owner);

            // set sale agent for the token contract
            await _tokenAsOwner.setSaleAgent(_tokenSale.address);
            // sending Eth to the contract
            await expectRevert(addr2.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            }), "TokenSale: It is not allowed to buy tokens for now");
            // checking balance for the contract and a buyer
            expect((await _tokenAsOwner.balanceOf(addr2.address)).toString()).to.equal("0");
        });

        it("9. Should check that user can't buy tokens if sale is over by reaching the total supply.", async function() {
            let _totalCap = 4;
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
            await _tokenContract.deployed()
            let _tokenSale = await TokenSale.deploy(_tokenContract.address, dateInThePast);
            await _tokenSale.deployed()
            let _tokenAsOwner = _tokenContract.connect(owner);
            // set sale agent for the token contract
            await _tokenAsOwner.setSaleAgent(_tokenSale.address);
            let _contractAsAddr4 = _tokenSale.connect(addr4);

            // sending Eth to the contract, buy 1 token1
            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");

            // sending Eth to the contract, buy 1
            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("2");

            // sending Eth to the contract, buy 1
            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("3");

            await expectRevert(addr2.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("1")
            }), "TokenSale: There is a limit of tokens for each transaction, try to buy less");

            // sending Eth to the contract, buy 1
            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("4");

            await expectRevert(addr2.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            }), "TokenSale: Sale is over by reaching the total supply");

            await expectRevert(addr2.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.06")
            }), "TokenSale: Sale is over by reaching the total supply");

            // checking balance for the contract again
            expect((await _tokenAsOwner.balanceOf(_tokenSale.address)).toString()).to.equal("0");
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("4");
        });
    });

    describe("Transfering", function() {
        it("1. Should check that tokens transfer to another user after sending ETH to the TokenSale contract", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);

            // sending Eth to the contract
            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            // checking balance for the contract and a buyer
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");
        })
    });

    describe("Ownerships", function() {
        it("1. Should check that ownership is changed after token buying", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);

            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("0");
            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");
            expect((await tokenAsOwner.ownerOf(1)).toString()).to.equal(addr3.address);

            // sending Eth to the contract, buy another token for 0.03 ETH
            await addr4.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await tokenAsOwner.balanceOf(addr4.address)).toString()).to.equal("1");
            expect((await tokenAsOwner.ownerOf(2)).toString()).to.equal(addr4.address);
        });
    });

    describe("Withdraw", function() {
        it("1. Should check that ETH balance is changed after withdraw", async function() {
            // set sale agent for the token contract
            await tokenAsOwner.setSaleAgent(tokenSale.address);

            // sending Eth to the contract
            await addr3.sendTransaction({
                to: tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })

            // checking balance for the contract and a buyer
            expect((await tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");

            expect((await ethers.provider.getBalance(tokenSale.address)).toString()).to.equal("10000000000000000");

            await saleAsOwner.setWithdrawAgent(addr5.address);
            await saleAsOwner.withdraw();

            expect((await ethers.provider.getBalance(tokenSale.address)).toString()).to.equal("0");
        });

        it("2. Should check that ETH balance is not changed after trying to withdraw Eth from contract under not owner account", async function() {
            await saleAsOwner.setWithdrawAgent(addr6.address);
            expect((await ethers.provider.getBalance(tokenSale.address)).toString()).to.equal("0");
            await expectRevert(contractAsAddr1.withdraw(), "Ownable: caller is not the owner");
            expect((await ethers.provider.getBalance(tokenSale.address)).toString()).to.equal("0");
        });

        it("3. Should check that withdraw address can not be changed by executing under not owner account", async function() {
            await expectRevert(contractAsAddr1.setWithdrawAgent(addr5.address), "Ownable: caller is not the owner");
        });
    });

    describe("Internal functionality", function() {
        it("1. Should check that id of token is consistent during issuing and token buying. Case 1", async function() {
            /*
                When the client will issue tokens to some addresses at first and then another user buy tokens
            */
            let _totalCap = 6;
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
            await _tokenContract.deployed()

            let _tokenSale = await TokenSale.deploy(_tokenContract.address, dateInThePast);
            await _tokenSale.deployed()

            let _tokenAsOwner = _tokenContract.connect(owner);
            // set sale agent for the token contract
            await _tokenAsOwner.setSaleAgent(_tokenSale.address);

            expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('0');
            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('1');
            expect((await _tokenAsOwner.balanceOf(addr3.address)).toString()).to.equal("1");

            await _tokenAsOwner.safeMint(addr6.address);
            await _tokenAsOwner.safeMint(addr6.address);
            expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('3');
            expect((await _tokenAsOwner.balanceOf(addr6.address)).toString()).to.equal("2");

            await addr5.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            })
            // it has already 2, result is 2 + 3
            expect((await _tokenAsOwner.balanceOf(addr5.address)).toString()).to.equal("1");
            expect((await _tokenAsOwner.getIdTracker()).toString()).to.equal('4');
            expect((await _tokenAsOwner.totalCap()).toString()).to.equal('6');
        });

        it("2. Should check that id of token is consistent during issuing and token buying. Case 2", async function() {
            /*
                When the client will issue tokens to some addresses after another user buy tokens
            */
            let _totalCap = 5;
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
            await _tokenContract.deployed();

            let _tokenSale = await TokenSale.deploy(_tokenContract.address, dateInThePast);
            await _tokenSale.deployed();

            let _tokenAsOwner = _tokenContract.connect(owner);
            let _tokenAsAddr4 = _tokenContract.connect(addr4);

            expect((await _tokenAsAddr4.totalCap()).toString()).to.equal('5');
            expect((await _tokenAsAddr4.getIdTracker()).toString()).to.equal('0');

            // set sale agent for the token contract
            await _tokenAsOwner.setSaleAgent(_tokenSale.address);

            expect((await _tokenAsAddr4.getIdTracker()).toString()).to.equal('0');

            await addr3.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            });
            expect((await _tokenAsAddr4.getIdTracker()).toString()).to.equal('1');
            expect((await _tokenAsAddr4.balanceOf(addr3.address)).toString()).to.equal("1");

            await _tokenAsOwner.safeMint(addr6.address);
            expect((await _tokenAsAddr4.getIdTracker()).toString()).to.equal('2');
            expect((await _tokenAsAddr4.balanceOf(addr6.address)).toString()).to.equal("1");

            await addr5.sendTransaction({
                to: _tokenSale.address,
                value: ethers.utils.parseEther("0.01")
            });

            expect((await _tokenAsAddr4.balanceOf(addr5.address)).toString()).to.equal("1");
            expect((await _tokenAsAddr4.getIdTracker()).toString()).to.equal('3');

            // check countable total supply
            expect((await _tokenAsAddr4.totalCap()).toString()).to.equal('5');
        });

        it("3. Should check that only owner of contracts can mint tokens", async function() {
            let _totalCap = 5;
            let _tokenContract = await NFT.deploy("Test NFT", "TNFT", _totalCap);
            await _tokenContract.deployed();

            let _tokenAsOwner = _tokenContract.connect(owner);
            let _tokenAsAddr3 = _tokenContract.connect(addr3);

            await _tokenAsOwner.safeMint(addr3.address);
            await expectRevert(
                _tokenAsAddr3.safeMint(addr3.address),
                "Ownable: caller is not the owner or the sale agent"
            );
        });
    });
});
