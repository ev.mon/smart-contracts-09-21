var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
chai.use(require("chai-bignumber")());
const {
    expectRevert
} = require("@openzeppelin/test-helpers");
// ethers - The ethers variable is available in the global scope
const {
    BigNumber
} = require("ethers");
const { set, reset } = require('mockdate');

function createArrs(starts, amount) {
    let _addresses = []
    let _amounts = []

    for(i = starts; i<amount; i++){
        if(`${i+1}`.toString().length == 1) {
            _addresses.push("0x000000000000000000000000000000000000000"+(i+1));
        } else if(`${i+1}`.toString().length == 2) {
            _addresses.push("0x00000000000000000000000000000000000000"+(i+1));
        } else if(`${i+1}`.toString().length == 3) {
            _addresses.push("0x0000000000000000000000000000000000000"+(i+1));
        }
        _amounts.push(1);
    }
    return [_addresses, _amounts];
}


describe("Vesting", function() {
    let owner, addr1, addr2, addr3, addr4, addr5, addr6;
    const badAddr = "0x0000000000000000000000000000000000000000";
    let Vesting, SimpleToken, tokenContract, vestingContract;
    let vAsOwner, vAsAddr1, vAsAddr2, vAsAddr3, vAsAddr4, vAsAddr6;

    const _date = new Date();
    const dateNow = (_date.setDate(_date.getDate()) / 1000) | 0;
    const dateNowForTimer = 1630875600000; // Mon Sep 06 2021 00:00:00 GMT+0300
    // dateNowForTimer + 4 days
    const dateNowForTimer2 = 1631221200000; // Fri Sep 10 2021 00:00:00 GMT+0300
    // dateNowForTimer + 8 days
    const dateNowForTimer3 = 1631566800000; // Tue Sep 14 2021 00:00:00 GMT+0300 
    // dateNowForTimer + 29 days
    const dateNowForTimer4 = 1633554000000; // Thu Oct 07 2021 00:00:00 GMT+0300
    // 23 week after dateNowForTimer 
    const dateNowForTimer23 = 1645221600000; // Fri Feb 19 2021 19:40:00 GMT+0200 

    // 26 week after dateNowForTimer 
    const dateNowForTimer26 = 1646431200000; // Sat Mar 05 2022 00:00:00 GMT+0200

    // 27 week after dateNowForTimer 
    const dateNowForTimer27 = 1646690400000; // Tue Mar 08 2022 00:00:00 GMT+0200

    const endDateForTimer = (_date.setDate(_date.getDate() + 26*7) / 1000) | 0;

    const _date1 = new Date();
    const dateInThePast = (_date1.setDate(_date1.getDate() - 10) / 1000) | 0;
    const dateInThePastForTimer = (_date.setDate(_date.getDate() - 10));

    const _dateNext = new Date();
    const nextDate = (_dateNext.setDate(_dateNext.getDate() + 10) / 1000) | 0;

    const _dateEnd = new Date();
    
    const afterEndDate = (_dateEnd.setDate(_dateEnd.getDate() + 26*8));

    let addresses;
    let amountTokens = ethers.utils.parseEther("100");
    let amountTokens1 = ethers.utils.parseEther("563");
    let amountTokens2 = ethers.utils.parseEther("0.026");
    let amountTokensAll = ethers.utils.parseEther("10000");

    let amounts = [
        amountTokens,
        amountTokens1,
        amountTokens2
    ];

    beforeEach(async function() {
        [owner, addr1, addr2, addr3, addr4, addr5, addr6, addr7, addr8, addr9, addr10, addr11, _] = await ethers.getSigners();
        addresses = [addr2.address, addr3.address, addr6.address];

        SimpleToken = await ethers.getContractFactory("SimpleToken");
        Vesting = await ethers.getContractFactory("Vesting");
        tokenContract = await SimpleToken.deploy();
        await tokenContract.deployed()
        vestingContract = await Vesting.deploy(tokenContract.address);
        await vestingContract.deployed()

        vAsOwner = vestingContract.connect(owner);
        vAsAddr1 = vestingContract.connect(addr1);
        vAsAddr2 = vestingContract.connect(addr2);
        vAsAddr3 = vestingContract.connect(addr3);
        vAsAddr4 = vestingContract.connect(addr4);
        vAsAddr6 = vestingContract.connect(addr6);
    });

    it("1. Should check base values after contract deployment", async function() {
        expect(await vestingContract.usedToken()).to.equal(tokenContract.address);
        expect((await vestingContract.totalTokensReleased()).toString()).to.equal("0");
        expect((await vestingContract.vestingPeriod()).toString()).to.equal("26");
        expect((await vestingContract.vestingPeriodTime()).toString()).to.equal("15724800");
    });

    it("2. Should revert during deplyment with bad address", async function() {
        await expectRevert(Vesting.deploy(badAddr), "Vesting: token address is not valid");
    });

    it("3. Should add Vesting info for addresses", async function() {
        let result = await vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("0");
        expect(result[1].toString()).to.equal("0");
        expect(result[2].toString()).to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("false");

        await vAsOwner.addVestingInfoBulk(addresses, amounts);

        result = await vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("0");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");
    });

    it("4. Should remove Vesting info for addresses", async function() {
        await vAsOwner.addVestingInfoBulk(addresses, amounts);

        let result = await vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("0");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");

        await vAsOwner.removeVesting(addr2.address);

        result = await vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("0");
        expect(result[1].toString()).to.equal("0");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("true");
        expect(result[4].toString()).to.equal("true");
    });

    it("5. Should release all of totalVesting amount for addressese during several weeks. Release flow", async function() {
        // ------ 1 dateNowForTimer -----
        set(dateNowForTimer);
        // console.log("part1");
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()

        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);
        let _vAsAddr3 = _vestingContract.connect(addr3);
        let _vAsAddr6 = _vestingContract.connect(addr6);

        expect((await tokenContract.balanceOf(_vestingContract.address)).toString()).to.equal("0");
        await tokenContract.mint(_vestingContract.address, amountTokensAll);
        expect((await tokenContract.balanceOf(_vestingContract.address)).toString()).to.equal(amountTokensAll.toString());

        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("0");
        expect((await tokenContract.balanceOf(addr3.address)).toString()).to.equal("0");
        expect((await tokenContract.balanceOf(addr6.address)).toString()).to.equal("0");
        expect((
            await tokenContract.balanceOf(_vestingContract.address)).toString()
        ).to.equal(amountTokensAll.toString());

        let result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("0");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");
        
        await _vAsAddr2.release();

        result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("3846153846153846153");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");

        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("3846153846153846153");
        expect((
            await tokenContract.balanceOf(_vestingContract.address)).toString()
        ).to.equal("9996153846153846153847");
        reset();

        // ------ 2 dateNowForTimer2 -----
        // console.log("part2");
        set(dateNowForTimer2);
        await expectRevert(
            _vAsAddr2.release(),
            "Vesting: Tokens were already sent this week"
        );
        
        reset();

        // ------ 3 dateNowForTimer3 -----
        // console.log("part3");
        set(dateNowForTimer3);
        await _vAsAddr2.release();
        result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("7692307692307692306");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");
        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("7692307692307692306");
        await expectRevert(
            _vAsAddr2.release(),
            "Vesting: Tokens were already sent this week"
        );

        await _vAsAddr3.release();
        result = await _vestingContract.vestingInfo(addr3.address);
        expect(result[0].toString()).to.equal("563000000000000000000");
        expect(result[1].toString()).to.equal("43307692307692307692");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");
        reset();


        // ------ 4 dateNowForTimer4 -----
        // console.log("part4");
        set(dateNowForTimer4);
        await _vAsAddr2.release();

        result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("19230769230769230765");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");

        await _vAsAddr6.release();
        result = await _vestingContract.vestingInfo(addr6.address);
        expect(result[0].toString()).to.equal("26000000000000000");
        expect(result[1].toString()).to.equal("5000000000000000");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");

        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("19230769230769230765");
        expect((await tokenContract.balanceOf(addr6.address)).toString()).to.equal("5000000000000000");
        expect((
            await tokenContract.balanceOf(_vestingContract.address)).toString()
        ).to.equal("9937456538461538461543");
        reset();


        // ------ 26 dateNowForTimer26 -----
        // console.log("part26");
        set(dateNowForTimer26);

        await _vAsAddr6.release();
        result = await _vestingContract.vestingInfo(addr6.address);
        expect(result[0].toString()).to.equal("26000000000000000");
        expect(result[1].toString()).to.equal("26000000000000000");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("true");
        expect(result[4].toString()).to.equal("true");

        await _vAsAddr2.release();
        result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("99999999999999999978");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("false");
        expect(result[4].toString()).to.equal("true");
        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("99999999999999999978");
        reset();


        // ------ 27 dateNowForTimer27 -----
        // console.log("part27");
        set(dateNowForTimer27);
        await _vAsAddr2.release();
        result = await _vestingContract.vestingInfo(addr2.address);
        
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("100000000000000000000");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("true");
        expect(result[4].toString()).to.equal("true");
        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("100000000000000000000");

        await expectRevert(
            _vAsAddr2.release(),
            "Vesting: amount of tokens already released"
        );

        await expectRevert(
            _vAsAddr6.release(),
            "Vesting: amount of tokens already released"
        );

        await _vAsAddr3.release();
        result = await _vestingContract.vestingInfo(addr3.address);
        expect(result[0].toString()).to.equal("563000000000000000000");
        expect(result[1].toString()).to.equal("563000000000000000000");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("true");
        expect(result[4].toString()).to.equal("true");

        await expectRevert(
            _vAsAddr3.release(),
            "Vesting: amount of tokens already released"
        );

        // check the balance of SC in the end
        // 10000 - 100 - 563 - 0.026 = 9336.974
        expect((
            await tokenContract.balanceOf(_vestingContract.address)).toString()
        ).to.equal("9336974000000000000000");
        reset();

        // 100 + 563 + 0.026 = 663.026
        expect((
            await _vestingContract.totalTokensReleased()).toString()
        ).to.equal("663026000000000000000");
        reset();
    });

    it("6. Should not remove Vesting info for address that is not in Vesting", async function() {
        await vAsOwner.addVestingInfoBulk(addresses, amounts);
        await expectRevert(
            vAsOwner.removeVesting(addr4.address),
            "Vesting: amount of tokens was not set yet for this address"
        );
    });

    it("7. Should not remove Vesting info for address that is already released", async function() {
        set(dateNowForTimer);
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()
        reset();
        await tokenContract.mint(_vestingContract.address, amountTokensAll);

        set(dateNowForTimer27);
        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);
        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        await _vAsAddr2.release();
        result = await _vestingContract.vestingInfo(addr2.address);
        expect(result[0].toString()).to.equal("100000000000000000000");
        expect(result[1].toString()).to.equal("100000000000000000000");
        expect(result[2].toString()).not.to.equal("0");
        expect(result[3].toString()).to.equal("true");
        expect(result[4].toString()).to.equal("true");

        await expectRevert(
            _vAsOwner.removeVesting(addr2.address),
            "Vesting: amount of tokens already released"
        );
        reset();
    });

    it("8. Should not remove Vesting info from address that is not the owner", async function() {
        await vAsOwner.addVestingInfoBulk(addresses, amounts);
        await expectRevert(
            vAsAddr2.removeVesting(addr2.address),
            "Ownable: caller is not the owner"
        );
    });
    it("9. Should not add Vesting info from address that is not the owner", async function() {});
    
    it("10. Should add Vesting info objects that are less then 100", async function() {
        let results = createArrs(0, 99);
        await vAsOwner.addVestingInfoBulk(results[0], results[1]);
        results = await vestingContract.vestingInfo("0x0000000000000000000000000000000000000099");
        expect(results[0].toString()).to.equal("1");
    });

    it("11. Should add Vesting info objects that are equal 100", async function() {
        let results = createArrs(0, 100);
        await vAsOwner.addVestingInfoBulk(results[0], results[1]);
        results = await vestingContract.vestingInfo("0x0000000000000000000000000000000000000100");
        expect(results[0].toString()).to.equal("1");
    });

    it("12. Should not add Vesting info objects that are more then 100", async function() {
        let results = createArrs(0, 101);
        await expectRevert(
            vAsOwner.addVestingInfoBulk(results[0], results[1]),
            "Vesting: length of provided addresses and amounts should be no more then 100"
        );
        results = await vestingContract.vestingInfo("0x0000000000000000000000000000000000000101");
        expect(results[0].toString()).to.equal("0");
    });

    it("13. Should not add Vesting info for addresses with not the same leght as for amounts", async function() {
        let _amounts = [1,2,3,4,5,6];
        await expectRevert(
            vAsOwner.addVestingInfoBulk(addresses, _amounts),
            "Vesting: length of provided addresses and amounts should be the same"
        );
    });

    it("14. Should not add Vesting info for addresses with a zero address", async function() {
        let _amounts = [1,2,4];
        let _addresses = [addr2.address, addr3.address, badAddr]
        await expectRevert(
            vAsOwner.addVestingInfoBulk(_addresses, _amounts),
            "Vesting: a receiver can not be a zero address"
        );
    });

    it("15. Should not add Vesting info for amounts with a zero amount", async function() {
        let _amounts = [1,0,4];
        let _addresses = [addr2.address, addr3.address, addr6.address];
        await expectRevert(
            vAsOwner.addVestingInfoBulk(_addresses, _amounts),
            "Vesting: amount can not be 0"
        );
    });

    it("16. Should add Vesting info tree times for different addresses", async function() {
        let address1 = "0x0000000000000000000000000000000000000003";
        let address2 = "0x0000000000000000000000000000000000000103";
        let address3 = "0x0000000000000000000000000000000000000203";
        let address4 = "0x0000000000000000000000000000000000000300";
        let results = await vestingContract.vestingInfo(address1);
        expect(results[0].toString()).to.equal("0");
        results = await vestingContract.vestingInfo(address2);
        expect(results[0].toString()).to.equal("0");
        results = await vestingContract.vestingInfo(address3);
        expect(results[0].toString()).to.equal("0");
        results = await vestingContract.vestingInfo(address4);
        expect(results[0].toString()).to.equal("0");

        results = createArrs(0, 100);
        await vAsOwner.addVestingInfoBulk(results[0], results[1]);
        results = createArrs(100, 200);
        await vAsOwner.addVestingInfoBulk(results[0], results[1]);
        results = createArrs(200, 300);
        await vAsOwner.addVestingInfoBulk(results[0], results[1]);

        results = await vestingContract.vestingInfo(address1);
        expect(results[0].toString()).to.equal("1");
        results = await vestingContract.vestingInfo(address2);
        expect(results[0].toString()).to.equal("1");
        results = await vestingContract.vestingInfo(address3);
        expect(results[0].toString()).to.equal("1");
        results = await vestingContract.vestingInfo(address4);
        expect(results[0].toString()).to.equal("1");
    });

    it("17. Should not release if balance of contract is 0", async function() {
        await vAsOwner.addVestingInfoBulk(addresses, amounts);
        await expectRevert(
            vAsAddr2.release(),
            "Vesting: balance of the contract is less then expected amount"
        );

    });

    it("18. Should retrieve tokens for the owner after 26 week", async function() {
        set(dateNowForTimer);
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()
        reset();

        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);
        let _vAsAddr3 = _vestingContract.connect(addr3);
        let _vAsAddr6 = _vestingContract.connect(addr6);

        await tokenContract.mint(_vestingContract.address, amountTokensAll);

        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        set(dateNowForTimer27);
        await _vAsAddr2.release();
        await _vAsAddr6.release();

        await expectRevert(
            _vAsAddr2.release(),
            "Vesting: amount of tokens already released"
        );

        await expectRevert(
            _vAsAddr6.release(),
            "Vesting: amount of tokens already released"
        );

        await _vAsAddr3.release();

        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        await _vAsOwner.retrieveAccessTokens("9336974000000000000000");
        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("9336974000000000000000");
        reset();
    });

    it("19. Should retrieve tokens for the owner before 26 week", async function() {
        set(dateNowForTimer);
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()
        reset();

        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);
        let _vAsAddr3 = _vestingContract.connect(addr3);
        let _vAsAddr6 = _vestingContract.connect(addr6);

        await tokenContract.mint(_vestingContract.address, amountTokensAll);

        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        set(dateNowForTimer26);
        await _vAsAddr2.release();
        await _vAsAddr6.release();
        await _vAsAddr3.release();

        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        await _vAsOwner.retrieveAccessTokens("20");
        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("20");
        reset();
    });

    it("20. Should not retrieve tokens for the simple user after 26 week", async function() {
        set(dateNowForTimer);
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()
        reset();

        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);

        await tokenContract.mint(_vestingContract.address, amountTokensAll);

        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        set(dateNowForTimer27);

        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        await expectRevert(
            _vAsAddr2.retrieveAccessTokens("300"),
            "Ownable: caller is not the owner"
        );
        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        expect((await tokenContract.balanceOf(addr2.address)).toString()).to.equal("0");
        reset();
    });

    it("21. Should not retrieve tokens if provided amount more then the contract has tokens", async function() {
        set(dateNowForTimer);
        let _vestingContract = await Vesting.deploy(tokenContract.address);
        await _vestingContract.deployed()
        reset();

        let _vAsOwner = _vestingContract.connect(owner);
        let _vAsAddr2 = _vestingContract.connect(addr2);

        await tokenContract.mint(_vestingContract.address, 20);

        await _vAsOwner.addVestingInfoBulk(addresses, amounts);

        set(dateNowForTimer27);

        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        await expectRevert(
            _vAsOwner.retrieveAccessTokens("300"),
            "Vesting: balance of the contract is less then provided amount"
        );
        expect((await tokenContract.balanceOf(owner.address)).toString()).to.equal("0");
        reset();
    });

    it("22. Should not add for the same addresses", async function() {
        let _amounts = [1,2,4];
        let _addresses = [addr2.address, addr3.address, addr3.address]
        await expectRevert(
            vAsOwner.addVestingInfoBulk(_addresses, _amounts),
            "Vesting: amount of tokens has already set for the user"
        );
    });

    it("23. Should not release if the address was not set in Vesting", async function() {
        await vAsOwner.addVestingInfoBulk(addresses, amounts);

        await expectRevert(
            vAsAddr4.release(),
            "Vesting: amount of tokens was not set for this address"
        );
    });
});
